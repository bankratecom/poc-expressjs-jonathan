var oaMort = require(_Services + '/feeds/oamortgage.js');
var url = require('url');

var baseUri = 'http://www.bankrate.com/ajax/dynamic-content/rate-tables/rt-mortgage-hp-dynamic.aspx';
var request = require('sync-request');


exports.index = function(req, res, next) {
   
    var oaData = oaMort.get('1,2,6');


    res.render('ratetable/mortgage_ratetable', {
        oa: oaData
    });

};

exports.getRates = function(req, res, next) {
	var url_parts = url.parse(req.url, true);
	var query = url_parts.query;

	var sendQuery = [];
	    sendQuery.push('prods=' + query.prods || '1');
	    sendQuery.push('loan=' + query.loan || '216000');
	    sendQuery.push('perc='+ query.perc || '20');
	    sendQuery.push('points=' + query.points || 'Zero');
	    sendQuery.push('fico='+ query.fico || '740');
	    
	    sendUri = '?' + sendQuery.join('&');

    sendRes = request('GET', baseUri + sendUri);

	res.json(JSON.parse(sendRes.getBody('utf8')));

};
    
