var storyFeeds = require(_Services + '/feeds/storyfeed.js');
var oaMort = require(_Services + '/feeds/oamortgage.js');


exports.getStory = function(req, res, next) {
    var slug = req.params.slug;
    var story = storyFeeds.getPostBySlug(slug);
    var oaData = oaMort.get('1,2,6');

    res.render('story/standard_story', {
        story: story,
        oa: oaData
    });

};
    
// test for commit