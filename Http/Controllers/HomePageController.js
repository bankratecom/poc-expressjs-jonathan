var storyFeeds = require(_Services + '/feeds/storyfeed.js');
var oaMort = require(_Services + '/feeds/oamortgage.js');

exports.index = function(req, res, next) {
    var stories = storyFeeds.getLatestPosts();
    var oaData = oaMort.get('1,2,6');

    res.render('home/home', {
        stories: stories,
        oa: oaData
    });

};
