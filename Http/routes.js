var HomePageController = require( _Controllers + '/HomePageController.js');
var StoryPageController = require( _Controllers + '/StoryPageController.js');
var MortgageRateTableController = require( _Controllers + '/MortgageRateTableController.js');

module.exports = function(app) {
	
	app.get('/', HomePageController.index);
	app.get('/story/:slug', StoryPageController.getStory);
	app.get('/funnel/mortgage', MortgageRateTableController.index);
	app.get('/ratedata/mortgage', MortgageRateTableController.getRates);

};