// drop downs
(function($){
	$(document).on('mouseenter mouseleave', '.main-nav-nav .dropdown', function(e){
		$(this).toggleClass('open');
	});
})(jQuery);

// Top story
car = (function($){
	var currentFrame = 0;
	var changeSpeed = 3000;
	var $tabs = $('.br-top-story-tab');

	var switchFrame = function swtichFrame(toFrame){
		

		//hide all 
		$('[class^="car-img"]').addClass('hide');

		// remove all active
		$tabs.removeClass('active');

		$tabs.eq(toFrame).addClass('active');
		
		$target = $($tabs.eq(toFrame).data('target'));

		$target.removeClass('hide');
	};

	var increaseFrame = function increaseFrame(){
		if(currentFrame + 1 == $tabs.length ) {
			currentFrame = 0;	
		} else {
			currentFrame++;
		}	
	};	

	var getCurrentFrame = function getCurrentFrame(){
		return currentFrame;
	};

	var setCurrentFrame = function setCurrentFrame(val){
		currentFrame = val;
		return val;
	};

	var setTimmer = function setTimmer(){
		window.moveCar = window.setInterval(function(){
			car.switchFrame(car.getCurrentFrame());
			car.increaseFrame();
		}, changeSpeed);
	};

	var clearTimmer = function clearTimmer(){
		clearInterval(window.moveCar);
	};

	setTimmer();

	$tabs.on('mouseenter', function(){
		clearTimmer();
		switchFrame($tabs.index($(this)));
		setCurrentFrame($tabs.index($(this)));
	}).on('mouseleave', setTimmer);
	
	return {
		switchFrame: switchFrame,
		increaseFrame: increaseFrame,
		getCurrentFrame: getCurrentFrame,
		setTimmer: setTimmer
	};
})(jQuery);

