(function(root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["json/EnvConfig", "meta", "vendor/s_code_brm", "platform"], factory);
    } else if (typeof exports === "object") {
        module.exports = factory(require("json/EnvConfig"), require("meta"), require("vendor/s_code_brm"), require('vendor/platform'));
    } else {
        root.bankrate = root.bankrate || {};
        root.bankrate.meta = root.bankrate.meta || {};
        root.s_gi = root.s_gi || {};
        root.platform = root.platform || {};
        root.bankrate.datacollector = factory(root.EnvConfig, root.bankrate.meta, root.s_gi, root.platform);
    }
}(this, function(config, meta, scode, platform) {
    window.BRDataQ = window.BRDataQ || [];

    var endpoint = "/DataCollectionModel.ajx";

    // Extend push 
    window.BRDataQ.push = function() {

        if (arguments[0] === "track") {
            processQueue.call(this);
        }

        return Array.prototype.push.apply(this, arguments);
    };

    var processQueue = function processQueue() {
        this.forEach(function(e, i, a) {
            if (typeof e === "object") {
                var da = buildDataObj();
                // TODO: add in extend/ overide method
                sendDataCollector.call(da);
            }
        });

        //TODO: Move this and omni clean hout to meta
        meta.cleanHouse();
    };

    var removeNullValues = function removeNullValues(obj){
        for (var k in obj){
            if (obj[k] === "" || obj[k] === null){
                delete obj[k];
            }
        }

        return obj;
    };

    var sendBeforeDataCollectorSendSignal = function sendBeforeDataCollectorSendSignal(obj) {
         var customevent = new CustomEvent('bankrate:track:beforeDataCollectorSend', {
            detail: {
                valuesObject: obj,
                jsonStr: encodeURIComponent(JSON.stringify(removeNullValues(obj)))
            }
        });


        document.dispatchEvent(customevent);
    };

    var sendDataCollector = function sendDataCollector() {
        sendBeforeDataCollectorSendSignal(this);
        var jsonstr = "JSON=" + encodeURIComponent(JSON.stringify(removeNullValues(this)));

        var xhttp = new XMLHttpRequest();


        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                console.log("datacollector sent");
            }
        };

        xhttp.open("POST", endpoint, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(jsonstr);
    };

    var extend = function extend() {
        for (var i = 1; i < arguments.length; i++)
            for (var key in arguments[i])
                if (arguments[i].hasOwnProperty(key))
                    arguments[0][key] = arguments[i][key];
        return arguments[0];
    };

    var getEventType = function getEventType() {
        return (meta.getPageView()) ? "Page Impression" : "Page Event";
    };

    var addCookieData = function addCookieData(obj) {
        var cookies = meta.getCookies();

        cookies.forEach(function(e, i, a) {
            obj['Cookie:' + e.name] = decodeURIComponent(e.value);
        });

        return obj;
    };

    var buildDataObj = function buildDataObj() {
        var dataObj = {
            "Type": 0,
            "Application": "Bankrate 3.0 Website",
            "Event": getEventType(),
            "Cobrand": meta.getMetaData('Site'),
            "CobrandType": meta.cobrandType(),
            "Referrer": document.referrer,
            "URL": document.URL,
            "Hostname": window.location.hostname,
            "QueryString": meta.getQueryString(),
            "PageName": window.location.pathname || 'default.aspx',
            "BrowserName": platform.name,
            "BrowserVersion": platform.version,
            "BrowserOS": navigator.platform,
            "UserAgent": navigator.userAgent,
            "Language": navigator.language || navigator.userLanguage,
            "Resolution": screen.width + 'x' + screen.height,
            "ScreenWidth": screen.width,
            "ScreenHeight": screen.height,
            "ColorDepth": screen.colorDepth,
            "JavaEnabled": navigator.javaEnabled(),
            "CookiesEnabled": navigator.cookieEnabled,
            "JavascriptVersion": meta.getJavascriptVersion(),
            "VisitorID": meta.getVID(),
            "BrowserSessionID": meta.getSID(),
            "SessionID": meta.getLSID(),
            "market": meta.getMetaData('market'),
            "PageMetadata:RI": meta.getRI(),
            "PageMetadata:PageType": meta.getMetaData('PageType'),
            "PageMetadata:AdKeyword": meta.getMetaData('AdKeyword'),
            "PageMetadata:Location": meta.getMetaData('Location'),
            "PageMetadata:Category": meta.getMetaData('Category'),
            "PageMetadata:AdArea": meta.getMetaData('AdArea'),
            "PageMetadata:Channel": meta.getMetaData('Channel'),
            "PageMetadata:PageID": meta.generatepageID(),
            "PageMetadata:Site": meta.getMetaData('Site'),
            "PageMetadata:Title": meta.getTitle(),
            "PageMetadata:Keywords": meta.getMetaData('Keywords'),
            "PageMetadata:Description": meta.getMetaData('Description'),
            "PageMetadata:Robots": meta.getMetaData('Robots'),
            "PageMetadata:DeviceType": meta.detectDeviceType(),
            "PageMetadata:ImpressionID": meta.getMetaData('ImpressionID'),
            "PageMetadata:PID": meta.getPID(),
            "PageMetadata:PVR": meta.getPVR(),
            "PageMetadata:AdTrack": meta.getAdTrack(),
            "PageMetadata:CCR": meta.getCCR(),
            "PageMetadata:Events": meta.generateEvents(),
            "PageMetadata:PurchaseID": meta.getPurchaseID(),
            "PageMetadata:ServerDuration": meta.getMetaData('ServerDuration'),
            "PageMetadata:ClientTimings1Connect": meta.getMetaData('ClientTimings1Connect'),
            "PageMetadata:ClientTimings2Load": meta.getMetaData('ClientTimings2Load'),
            "PageMetadata:ClientTimings3Render": meta.getMetaData('ClientTimings3Render'),
            "PageMetadata:ClientTimings4Duration": meta.getMetaData('ClientTimings4Duration')
        };

        dataObj = addCookieData(dataObj);

        return dataObj;

    };


    return {
        buildDataObj: buildDataObj
    };
}));