
var RateTable = React.createClass({
        loadRateData: function() {
          $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            data: $('.ratetable-gearbox form').serialize(),
            success: function(data){
              this.setState({data:data});
              var loading = document.getElementById('loading');
              loading.className = 'hide';
              $('#loan').val(data.response.global.loanAmount);
            }.bind(this),
             error: function(xhr, status, err) {
              console.error(this.props.url, status, err.toString());
            }.bind(this)
          });
        },
         getInitialState: function() {
          return {data: {}};
        },
         componentDidMount: function() {
          var _ = this;
          $('#update-table').on('click', function(event){
            event.preventDefault();
            var loading = document.getElementById('loading');
              loading.className = '';
            _.loadRateData();   
            
          });
          this.loadRateData();          
        },  

         render: function() {
          return (
            <div className="ratetable-body">            
              <div className="header-row row">
                <div className="ratetable-header-item col-sm-3 col-md-2 col-lg-3">Lender</div> 
                <div className="ratetable-header-item col-sm-2 col-md-2 col-lg-2">APR</div>
                <div className="ratetable-header-item col-sm-2 col-md-2 col-lg-2">Rate</div>
                <div className="ratetable-header-item col-sm-2 col-md-2 col-lg-2">Est Mo Payment</div>
                <div className="ratetable-header-item col-sm-3 col-md-3 col-lg-3">Have Qestions?</div>
              </div>
              <RateTableRow data={this.state.data} /> 
               
            </div>             
          );
        }
      });

      var RateTableRow = React.createClass({         
        epClick: function(i,clickType,choverride,rediroverride,profilelink,cobrand,e){
          e.preventDefault();

          var cobrand = 'br3';
          var cpcData = this.props.data.response.result1[i].cpc.split('||')[0];

          var redirectTo = cpcData;
          redirectTo += "&c=";
          redirectTo += i;
          redirectTo += ',';
          redirectTo += clickType;
          redirectTo += ',';
          redirectTo += choverride;
          redirectTo += ',';
          redirectTo += rediroverride;
          redirectTo += ',';
          redirectTo += ((typeof profilelink != 'undefined') ? encodeURIComponent(profilelink) : '');
          redirectTo += ',';
          redirectTo += ((typeof cobrand != 'undefined') ? cobrand : '');

          // adding in date
          var today = new Date(); var year = today.getFullYear(); var month = today.getMonth() + 1; var date = today.getDate();
          var hour = today.getHours(); var minute = today.getMinutes(); var second = today.getSeconds(); var millisecond = today.getMilliseconds();
          var currentDT = month + '/' + date + '/' + year + ' ' + hour + ':' + minute + ':' + second + '.' + millisecond;
          redirectTo += '&click_date=' + currentDT;

          // adding in href 
          redirectTo += '&ref_url=' + escape(window.location.href);

          // adding impression id and purchase id
          var purID = false,
              impID = false;

          redirectTo += (purID) ? '&pur_id=' + purID : '';
          redirectTo += (impID) ? '&imp_id=' + impID : '';

          var win = window.open(redirectTo, 'openWindow' );

          try {
              win.focus();
          } catch (e) {
              window.location = redirectTo;
          }

        },
         render: function() {
          var _ = this
          var rows;
          if(this.props.data.response)            
            rows = this.props.data.response.result1.map(function(row,i){
              var image = row.listingType == 'paid' ? <img src={'http://www.brimg.net/' + row.img} onClick={_.epClick.bind(_,i, '8', '','', '', '')}/> : <a href="#" onClick={_.epClick.bind(_,i, '2', '','', '', '')}>{row.lender}</a> ;
              var nextButton = row.listingType == 'paid' ? <button className="btn btn-primary pull-right ratetable-button" onClick={_.epClick.bind(_,i, '11', '','', '', '')}  >Next</button> : '' ;
              var phone = row.listingType == 'paid' ? <div><span className="ratetable-phonenubmer">{row.phone}</span></div> : '' ;
              return (
                <div className={row.listingType + ' inner-row row'} key={row.id}>              
                  <div className="ratetable-column ratetable-image col-sm-3 col-md-3 col-lg-3">{image }</div>                   
                  <div className="ratetable-column ratetable-rate-info col-sm-2 col-md-2 col-lg-2">
                    <div>{row.apr}</div>
                    <div>{row.fees}</div>
                    <div>{row.date}</div>
                  </div>                  
                  <div className="ratetable-column ratetable-points col-sm-2 col-md-2 col-lg-2">
                    <div>{row.rate} at {row.points}pts</div>
                    <div>{row.lock} day lock</div>
                  </div>
                  <div className="ratetable-column ratetable-payment col-sm-2 col-md-2 col-lg-2">
                    ${row.payment}
                    &nbsp;
                  </div>
                  <div className="ratetable-column ratetable-phone col-sm-3 col-md-3 col-lg-3">
                    {phone}  
                    &nbsp;                  
                    {nextButton}
                  </div>                  
                </div>
              )
            });
          else {
            rows = <div></div>
          }
          return (
            <div>{rows}</div>
          )
        }
      });
    
     
      var RateTableComponent = ReactDOM.render(
        <RateTable url="/ratedata/mortgage" />,
        document.getElementById('content')
      );



