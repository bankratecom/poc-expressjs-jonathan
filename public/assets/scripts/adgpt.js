(function(root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["json/EnvConfig", "meta", "omniture"], factory);
    } else if (typeof exports === "object") {
        module.exports = factory(require("json/EnvConfig"), require("meta"), require('"omniture"'));
    } else {
        root.bankrate = root.bankrate || {};
        root.bankrate.meta = root.bankrate.meta || {};
        root.bankrate.omniture = root.bankrate.omniture || {};
        root.bankrate.adgpt = factory(root.EnvConfig, root.bankrate.meta, root.bankrate.omniture);
    }
}(this, function(config, meta, omniture) {
    window.BROmniQ = window.BROmniQ || [];
    window.BRDataQ = window.BRDataQ || [];
    var injectAds = window.injectAds || [];
    var totalCount = 0;
    var refreshingAdsOnClose = true;
    var interstitialFlag = false;
    var delayPace = 500;
    var adsran = false;

    var isInterstitial = function isInterstitial() {
        var elm = document.querySelector('[data-dfp="interstitial"]');

        if (elm) interstitialFlag = true;

        return (elm) ? true : false;
    };

    var filterByConditions = function filterByConditions(elm) {
        var flag = true;
        var elmConditions = elm.getAttribute('data-condition'); // from data-condition attribute can have multiple (,) delimited

        if (elmConditions !== null) {
            elmConditions = elmConditions.split(',');
            elmConditions.forEach(function(cond, index, array) {
                if (conditions[cond.toLowerCase()] === false) {
                    flag = false;
                }
            });
        }

        return flag;
    };

    var findSlotPlaceHolders = function findSlotPlaceHolders(selector) {
        // Self and inside search
        selector = selector || "";
        var slots = [].slice.call(doc.querySelectorAll(selector + '[data-dfp]' + ', ' + selector + ' [data-dfp]')).filter(filterByConditions);

        return slots;
    };

    var generateEnvironmentTarget = function generateEnvironmentTarget() {
        var domain = window.location.hostname;
        var target = "prod";

        if ((/sandbox/gi).test(domain)) {
            target = "sandbox";
        }
        if ((/qa|xq/gi).test(domain)) {
            target = "qa";
        }
        if ((/dev|local/gi).test(domain)) {
            target = "dev";
        }

        return target;
    };

    var injectAdsBeforeCall = function injectAdsBeforeCall() {
        if (injectAds.length > 0) {
            //console.log('Injected Ads: There are ads to inject');
            injectAds.forEach(function(e, i, a) {
                var dfp = e.dfp || '';
                var cond = e.condition || '';
                var sty = e.style || '';
                var siz = e.sizes || '';

                var pos = (e.insertLocation.position == 'after') ? 'afterend' : 'beforebegin';
                var htmlString = '<div data-dfp="' + dfp + '" id="dfp_' + dfp + '" data-condition="' + cond + '" style="' + sty + '" data-sizes="' + siz + '"></div>';
                // Does insert location exist
                console.log('Injected Ads: Looking for spot to inject for ad ' + e.dfp);
                if (doc.querySelectorAll(e.insertLocation.selector).length > 0) {
                    //console.log('Injected Ads: Spot ' + e.insertLocation.selector + ' found');
                    doc.querySelectorAll(e.insertLocation.selector)[0].insertAdjacentHTML(pos, htmlString);
                    //console.log('Injected Ads: ' + e.dfp + ' ad injected');
                } else if (e.force && e.force === true) {
                    var tmp = document.createElement("div");
                    tmp.innerHTML = htmlString;
                    var childNodes = [].slice.call(tmp.childNodes);
                    childNodes.forEach(function(ce, ci, ca) {
                        doc.body.appendChild(ce);
                    });
                    //console.log('Injected Ads: Spot ' + e.insertLocation.selector + ' not found forcing injection for ad ' + e.dfp);
                } else {
                    //console.log('Injected Ads: Spot ' + e.insertLocation.selector + ' not found');
                }
            });
        }
    };

    var getRandomInt = function getRandomInt() {
        return Math.floor(Math.random() * 10000000000);
    };

    var isDomNode = function isDomNode(elm) {
        return (elm.nodeName) ? true : false;
    };

    var appendID = function appendID(elms) {

        for (var i = elms.length - 1; i >= 0; i--) {
            elms[i].id = idprefix + getRandomInt() + '-' + i;
        }

        return elms;
    };

    var extractTrackDiv = function extractTrackDiv(id, event) {
        var placeholder = document.getElementById(id),
            slotTrackInfo = null;
        var placeholderIframe = placeholder.querySelector('[id$="__container__"] > iframe');
        if (placeholderIframe) {
            var placeholderIframeElm = placeholderIframe;
            var trackDiv = placeholderIframeElm.contentWindow.document.getElementById('brtrack');
            if (trackDiv) {
                slotTrackInfo = {
                    lineitemid: trackDiv.getAttribute('data-line-item-id'),
                    advertiserid: trackDiv.getAttribute('data-advertiser-id'),
                    creativeid: trackDiv.getAttribute('data-creative-id'),
                    cpm: trackDiv.getAttribute('data-cpm'),
                    cpc: trackDiv.getAttribute('data-cpc'),
                    cpd: trackDiv.getAttribute('data-cpd'),
                    position: event.slot.getTargeting('pos')
                };

                if (event && event.size) {
                    slotTrackInfo.width = event.size[0];
                    slotTrackInfo.height = event.size[1];
                }

                if (Number(slotTrackInfo.cpc) > 0) {
                    attachCpcClickEvent(placeholderIframeElm, slotTrackInfo.cpc, event);
                }


                meta.setCPD(slotTrackInfo.lineitemid);


            }
        }

        return slotTrackInfo;
    };

    var processRevenue = function processRevenue(event) {
        // event argument contains ad info too
        var trackObj = null;
        var position = event.slot.getTargeting('pos');
        var id = event.slot.getSlotElementId();

        adsran = true;

        activeSlots.setFinishedRenderCount();
        trackObj = extractTrackDiv(id, event);

        if (trackObj) {
            trackinfo.push(trackObj);
        } else {
            // IE 9, 10
            tryAgain(id, event);
        }

        // Interstitial 
        if (interstitialFlag && position[0] === "interstitial" && hasSeenInterstitial() === false) {
            runInterstitialAd();
        }

        if (totalCount == activeSlots.finishedRenderCount) {

            // Set cookie expires every 24 hours
            if (interstitialFlag && hasSeenInterstitial() === false) {
                setInterstitialCookie();
            }

            //send omniture 
            activeSlots.finishedRenderCount = 0;
            sendOmniture();
            sendTrackedAdSignal();
            sendDataCollector();
            cleanTrackinfo();
        }
    };

    var isInterstitialEmpty = function isInterstitialEmpty() {
        var int = document.querySelector('[data-dfp="interstitial"] [id$="__container__"] > iframe');

        return (int.contentWindow.document.body.childNodes.length === 0) ? true : false;

    };

    var sendTrackedAdSignal = function sendTrackedAdSignal() {
        var TotRev = 0;
        var adpos = {};

        var adposArray = trackinfo.map(function(e) {
            TotRev += parseFloat(e.cpm);
            var info = {
                dim: 'WIDTH=' + e.width + ' HEIGHT=' + e.height,
                ccid: e.lineitemid,
                campid: e.advertiserid,
                cpm: e.cpm,
                cpc: e.cpc,
                cpd: e.cpd
            };
            obj = {};
            obj[e.position] = JSON.stringify(info);
            adpos[e.position] = JSON.stringify(info);

            return obj;
        });

        totRev = TotRev / 1000;
        var pvrStr = meta.getPVR();

        var customevent = new CustomEvent('bankrate:ads:tracked', {
            detail: {
                adpos: adpos,
                totRev: totRev,
                pvrStr: pvrStr
            }
        });

        document.dispatchEvent(customevent);
    };

    var sendOmniture = function sendOmniture() {
        var pvr = buildPVR();
        var omniObj = {};
        meta.setPVR(pvr);
        window.BROmniQ.push(omniObj);
        window.BROmniQ.push("track");

        console.log('PVR: ' + calculatePVR() / 1000);
    };

    var sendDataCollector = function sendDataCollector() {
        var dataObj = {};
        var adtrack = buildAdTrack();
        window.BRDataQ.push(dataObj);
        window.BRDataQ.push("track");
        console.log("AdTrack: " + adtrack);
    };

    var tryAgain = function tryAgain(id, event) {
        delayPace += 500;
        setTimeout(function(id, event) {
            var deplayedtrackInfo = extractTrackDiv(id, event);
            if (deplayedtrackInfo) {
                trackinfo.push(deplayedtrackInfo);
                omniture.setPEV2(event.slot.getTargeting('pos'));
                sendOmniture();
                sendDataCollector();
                cleanTrackinfo();

                if (interstitialFlag && event.slot.getTargeting('pos')[0] === "interstitial" && deplayedtrackInfo) {
                    runInterstitialAd();
                }

            }
        }, delayPace, id, event);
    };

    var attachCpcClickEvent = function attachCpcClickEvent(elm, cpc, event) {
        elm.contentWindow.document.addEventListener('click', function clickCPCAd(event) {

            // add click to omniture queue
            var pvr = "PVR;" + meta.getMetaData('AdArea') + ";1;" + cpc + ";event23=" + cpc;

            meta.setPVR(pvr);
            omniture.setPEV2('Ads Call');
            meta.setEvents('event14');

            var omniObj = {};
            window.BROmniQ.push(omniObj);
            window.BROmniQ.push("track");

            var dataObj = {};
            window.BRDataQ.push(dataObj);
            window.BRDataQ.push("track");

            console.log('PVR: ' + cpc);

        }, true, true);
    };

    var cleanTrackinfo = function cleanTrackinfo() {
        trackinfo = [];
    };

    var calculatePVR = function calculatePVR() {
        var pvr = 0;
        trackinfo.forEach(function(e, i, a) {
            pvr += parseFloat(e.cpm);
        });

        return pvr;
    };

    var buildPVR = function buildPVR() {
        var str = "PVR;" + meta.getMetaData('AdArea') + ";1;" + (calculatePVR() / 1000) + ";event23=" + calculatePVR();
        meta.setPVR(str);
        return str;
    };

    var buildAdTrack = function buildAdTrack() {
        var TotRev = 0;
        var adtrack = {
            adsTrack: trackinfo.map(function(e) {
                TotRev += parseFloat(e.cpm);
                return {
                    dim: 'WIDTH=' + e.width + ' HEIGHT=' + e.height,
                    ccid: e.creativeid,
                    campid: e.lineitemid,
                    cpm: e.cpm,
                    cpc: e.cpc,
                    cpd: e.cpd
                };
            })
        };

        adtrack.TotRev = TotRev / 1000;

        var jsonStr = JSON.stringify(adtrack);

        meta.setAdTrack(jsonStr);

        return jsonStr;
    };

    var convertPlaceholderToSlot = function convertPlaceholderToSlot(elms) {
        totalCount = elms.length;

        // Set unique ID
        elms = appendID(elms);

        for (var i = elms.length - 1; i >= 0; i--) {
            // Get sizes if its there
            var size = (elms[i].getAttribute('data-sizes') && elms[i].getAttribute('data-sizes').length > 0) ? elms[i].getAttribute('data-sizes').split(',').map(function(e) {
                return e.split('x').map(function(ie) {
                    return Number(ie);
                });
            }) : sizes;
         
                var gpt = googletag.defineSlot(segments, size, elms[i].id)
                    .setTargeting('adkeyword', adKeywords)
                    .setTargeting('site', cobrand)
                    .setTargeting('pos', elms[i].getAttribute('data-dfp'))
                    .setTargeting('environment', environment)
                    .setTargeting('pageid', meta.generatepageID())
                    .setTargeting('adarea', adArea)
                    .addService(googletag.pubads())
                    .addService(googletag.content())
                    .setCollapseEmptyDiv(true);

                activeSlots.add = gpt;
   

        }


        return elms;
    };

    var attachProcessRevEvent = function attachProcessRevEvent() {
    
            googletag.pubads().addEventListener('slotRenderEnded', processRevenue);
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();

    };

    var initDisplay = function initDisplay(elm) {
        var elms, i;
        // Poly morph

        if (typeof elm == 'string') { //init by div id
     
                googletag.display(elm);

        } else if (Array.isArray(elm)) { // array of ids
            elms = elm;
            for (i = elms.length - 1; i >= 0; i--) {
               
                    googletag.display(elms[i].id);
            
            }
        } else { // collection from active slots
            elms = activeSlots.all;
            for (i = elms.length - 1; i >= 0; i--) {
                
                    googletag.display(elms[i].l.m);
            
            }
        }

    };

    var hasSeenInterstitial = function hasSeenInterstitial() {
        return (/brinterstitial/).test(document.cookie);
    };

    var setInterstitialCookie = function setInterstitialCookie() {
        var now = new Date();
        var time = now.getTime();
        time += 86400000;
        now.setTime(time);
        document.cookie =
            'brinterstitial=true' +
            '; expires=' + now.toUTCString() +
            '; path=/';

        return true;
    };

    var runInterstitialAd = function runInterstitial() {
        var int = document.querySelector('[data-dfp="interstitial"] [id$="__container__"] > iframe');

        if (int.contentWindow.document.readyState != "loading") {

            if (isInterstitialEmpty()) {
                clearInterstitialAd();
            } else {
                setTimeout(clearInterstitialAd, 7000);
            }

        } else {

            int.contentWindow.document.addEventListener("DOMContentLoaded", function(event) {

                if (isInterstitialEmpty()) {
                    clearInterstitialAd();
                } else {
                    setTimeout(clearInterstitialAd, 7000);
                }

            });
        }

        var container = document.querySelector('.br-interstitial-container');

        if (!(/br-active-interstitial/gmi).test(document.body.className)) {
            document.body.className += ' br-active-interstitial';
        }

        if (container) {
            container.className = container.className.replace("hide", "");
        }
    };

    var clearInterstitialAd = function clearInterstitialAd() {
        var container = document.querySelector('.br-interstitial-container');

        if (container) {
            container.className = container.className += " hide";
            document.body.className = document.body.className.replace('br-active-interstitial', '');
        }
    };

    var refreshAds = function refreshAds(event, adSlotID) {
        if (!refreshingAdsOnClose) {
            refreshAdsOnClose = true;
            return false;
        }

        adSlotID = adSlotID || null;

        if (adSlotID === null) {
            meta.setPageView(true);
            totalCount = activeSlots.refreshingAds.length;
            googletag.pubads().refresh(activeSlots.refreshingAds);
        } else {
            var slotArray = [];
            if (!Array.isArray(adSlotID)) {
                slotArray.push(adSlotID);
            } else {
                slotArray = adSlotID;
            }
            totalCount = slotArray.length;
            googletag.pubads().refresh(slotArray);
        }
    };

    var shouldModalCloseRefreshAds = function shouldModalCloseRefreshAds() {
        var elm = document.querySelector('.modal.in');
        var data = elm.dataset;
        refreshingAdsOnClose = (elm && data.refreshAdsOnClose && data.refreshAdsOnClose === "false") ? false : true;
    };

    var populateModalEventName = function populateModalEventName() {
        var eventName = '';
        var elm = document.querySelector('.modal.in');
        if (elm && elm.dataset && elm.dataset.name) {
            eventName = elm.dataset.name;
            omniture.setPEV2(eventName);
        }

        return eventName;
    };

    var runModalAds = function runModalAds() {
        shouldModalCloseRefreshAds();
        populateModalEventName();

        totalCount = 0;
        var modalAds = findSlotPlaceHolders('.modal.in');
        var isAlreadyASlot = [];
        totalCount = modalAds.length;

        modalAds.forEach(function(e, i, a) {
            activeSlots.all.forEach(function(c, ci, ca) {
                if (e.id === c.l.m) {
                    isAlreadyASlot.push(c);
                }
            });

            activeSlots.nonRefreshingAdsPos.push(e.id);
        });

        if (isAlreadyASlot.length > 0) {
            refreshAds(null, isAlreadyASlot);
        } else {
            convertPlaceholderToSlot(modalAds);
            initDisplay(modalAds);
        }

    };

    var init = function init() {
        if ((/noads/i).test(window.top.location.hash) === false) {
            detectAdBlock();
            checkIfAdsRan();
            setAttribute();
            injectAdsBeforeCall();
            isInterstitial();
            deactivateInterstitialIf();
            var foundSlots = findSlotPlaceHolders();
            convertPlaceholderToSlot(foundSlots);
            attachProcessRevEvent();
            initDisplay(foundSlots);
        }

        if(areThereAds() === false) {
            sendOmniture();
            sendDataCollector();
        }

    };

    var loadAd = function loadAd(selector) {
        // For double ad.
        googletag.pubads().clear();
        var foundSlots = findSlotPlaceHolders(selector);
        convertPlaceholderToSlot(foundSlots);
        initDisplay(foundSlots);
    };

    var checkIfAdsRan = function checkIfAdsRan() {
        setTimeout(function() {
            if (window.bankrate.adgpt.areThereAds() === true && window.bankrate.adgpt.didAdsRun() === false) {
                console.error('Bankrate ads did not initially load. Trying again.');
                bankrate.meta.cleanHouse();
                bankrate.adgpt.activeSlots.removeAll();
                bankrate.adgpt.activeSlots.clearFinishedRenderCount();
                totalCount = 0;
                trackinfo = [];
                loadAd();
            }
        }, 4000);
    };

    var didAdsRun = function didAdsRun() {
        return adsran;
    };

    var areThereAds = function areThereAds() {
        var ads = findSlotPlaceHolders();
        return (ads && ads.length > 0) ? true : false;
    };

    var deactivateInterstitialIf = function deactivateInterstitialIf() {
        if (conditions.ismobile && interstitialFlag || (interstitialFlag && hasSeenInterstitial() === true)) {
            clearInterstitialAd();
        }
    };

    var detectAdBlock = function detectAdBlock() {
        var test = document.createElement('div');
        test.innerHTML = '&nbsp;';
        test.className = 'adsbox';
        document.body.appendChild(test);
        window.setTimeout(function() {
            if (test.offsetHeight === 0) {
                console.error('Bankrate: Ad blocker detected');
                if (interstitialFlag) {
                    clearInterstitialAd();
                    sendOmniture();
                    sendDataCollector();
                }
            }
            test.remove();
        }, 100);
    };

    var ready = function ready(fn) {
        if (document.readyState != 'loading') {
            fn();
        } else {
            document.addEventListener('DOMContentLoaded', fn);
        }
    };

    // Load suite and environment vars
    var trackinfo = [];
    var doc = document;
    var suite = config.gptsuite;
    var site = config.gptsite;

    var adArea, pageType, pageID, adKeywords, cobrand, segments, environment;

    var setAttribute = function setAttribute() {
        adArea = meta.getMetaData("AdArea") || "";
        adArea = adArea.replace(/\(|\)|\-|\s/g, '');
        pageType = meta.getMetaData("PageType") || "";
        pageID = meta.getMetaData("PageID") || "";
        adKeywords = meta.getAdKeywords();
        cobrand = meta.getMetaData("Site") || "";
        segments = [suite, site, adArea, pageType].join('/').replace(/\(|\)|\-|\s/g, '');
        environment = generateEnvironmentTarget();
    };

    var idprefix = "br-ad-";
    var sizes = [
        [1, 1],
        [160, 600],
        [180, 150],
        [120, 90],
        [292, 150],
        [300, 100],
        [300, 250],
        [300, 50],
        [300, 600],
        [300, 75],
        [320, 35],
        [320, 50],
        [320, 50],
        [360, 20],
        [607, 61],
        [615, 122],
        [666, 124],
        [728, 90],
        [800, 500],
        [970, 250],
        [970, 90],
        [1600, 1000]
    ];

    var conditions = {
        get ismobile() {
            var screenWith = window.screen.width;
            var mobileClass = doc.querySelector('.mobile-view');

            return ((screenWith <= 640) || (mobileClass)) ? true : false;
        },
        get isdesktop() {
            var screenWith = window.screen.width;
            var mobileClass = doc.querySelector('.mobile-view');

            return ((screenWith > 640) && (mobileClass === null)) ? true : false;
        },
        get ismodalopen() {
            var modal = doc.querySelectorAll('.modal.in').length;
            return (modal > 0) ? true : false;
        },
        get isstorypage() {
            var cssclass = doc.querySelectorAll('.storypage').length;
            return (cssclass > 0) ? true : false;
        },
        get isplainstorypage() {
            var cssclass = doc.querySelectorAll('.storypage').length;
            var andnotclasses = doc.querySelectorAll('div[class^="slideshow-controls-"], .brSlide, div[class^="gallery-"]').length;
            return (cssclass > 0 && andnotclasses === 0) ? true : false;
        },
        get isslideshow() {
            var cssclass = doc.querySelectorAll('.storypage').length;
            var andclasses = doc.querySelectorAll('div[class^="slideshow-controls-"], .brSlide').length;
            return (cssclass > 0 && andclasses > 0) ? true : false;
        },
        get isgallery() {
            var cssclass = doc.querySelectorAll('.storypage').length;
            var andclasses = doc.querySelectorAll('div[class^="gallery-"]').length;
            return (cssclass > 0 && andclasses > 0) ? true : false;
        },
        get isblog() {
            var cssclass = doc.querySelectorAll('.blog-post-content').length;
            return (cssclass > 0) ? true : false;
        },
        get isemptyratetable() {
            var rows = [].slice.call(document.querySelectorAll(".br-rate-table-row.paid"));

            return (rows.length === 0) ? true : false;
        },
        get interstitial() {
            return (hasSeenInterstitial()) ? false : true;
        }
    };

    var activeSlots = {
        activeSlotsSet: [],
        finishedRenderCount: 0,
        nonRefreshingAdsPos: ['interstitial', 'lightbox'],
        get nonRefreshingAds() {
            var _ = this;
            var set = this.activeSlotsSet.filter(function(e) {
                return (_.nonRefreshingAdsPos.indexOf(e.o.pos[0]) >= 0) ? true : false;
            });
            return set;
        },
        get refreshingAds() {
            var _ = this;
            var set = this.activeSlotsSet.filter(function(e) {
                return (_.nonRefreshingAdsPos.indexOf(e.o.pos[0]) >= 0) ? false : true;
            });
            return set;
        },
        get all() {
            return this.activeSlotsSet;
        },
        get allSlotNames() {
            var slots = [];
            this.activeSlotsSet.forEach(function(e, i, a) {
                slots.push(e.l.m);
            });

            return slots;
        },
        get count() {
            return this.activeSlotsSet.length;
        },
        setFinishedRenderCount: function setFinishedRenderCount() {
            this.finishedRenderCount += 1;
        },
        clearFinishedRenderCount: function clearFinishedRenderCount() {
            this.finishedRenderCount = 0;
        },
        set add(o) {
            this.activeSlotsSet.push(o);
        },
        set removeByIndex(index) {
            if (!isNaN(index)) {
                delete this.activeSlotsSet[index];
            }
        },
        set removeBySlotName(slots) {
            var _ = this;
            //Polymorth
            if (typeof slots === "string") {
                _.removeBySlotNameMethod(slots);
            } else if (Array.isArray(slots)) {
                slots.forEach(function(e, i, a) {
                    _.removeBySlotNameMethod(e);
                });
            }

        },
        removeBySlotNameMethod: function removeBySlotNameMethod(slotName) {
            this.activeSlotsSet.forEach(function(e, i, a) {
                if (e.l.m === slotName) delete a[i];
            });
        },
        removeAll: function removeAll() {
            this.activeSlotsSet = [];
        }
    };

    // Events
    document.addEventListener("DOMContentLoaded", init);


    return {
        findSlotPlaceHolders: findSlotPlaceHolders,
        activeSlots: activeSlots,
        convertPlaceholderToSlot: convertPlaceholderToSlot,
        initDisplay: initDisplay,
        refreshAds: refreshAds,
        conditions: conditions,
        loadAd: loadAd,
        init: init,
        extractTrackDiv: extractTrackDiv,
        runModalAds: runModalAds,
        runInterstitialAd: runInterstitialAd,
        clearInterstitialAd: clearInterstitialAd,
        adsran: adsran,
        didAdsRun: didAdsRun,
        areThereAds: areThereAds,
        isInterstitial: isInterstitial,
        isInterstitialEmpty: isInterstitialEmpty
    };
}));