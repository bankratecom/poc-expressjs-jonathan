(function(root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["json/EnvConfig", "vendor/hashids',"], factory);
    } else if (typeof exports === "object") {
        module.exports = factory(require("json/EnvConfig"), require('vendor/hashids'));
    } else {
        root.bankrate = root.bankrate || {};
        root.Hashids = root.Hashids || {};
        root.bankrate.meta = factory(root.EnvConfig, root.Hashids);
    }
}(this, function(config) {
    var pageview = true;
    var pvr = null;
    var ccr = null;
    var cpd = [];
    var brri = null;
    var brevents = [];
    var purchaseID;
    var adtrack;
    var clickedRow;
    var ServerDuration = "0";
    var ClientTimings1Connect;
    var ClientTimings2Load;
    var ClientTimings3Render;
    var ClientTimings4Duration;

    var getMetaData = function getMetaData(key) {
        metaDataValue = null;

        //v3
        metaDataValue = (window.PageManager && PageManager.PageMetaData && PageManager.PageMetaData.Values && PageManager.PageMetaData.Values[key]) ? PageManager.PageMetaData.Values[key] : null;

        //v4
        if (metaDataValue === null) {
            var v4meta = document.querySelector('meta[property="' + key + '"], meta[name="' + key + '"], meta[property="PageMetadata:' + key + '"], meta[name="' + key.toLowerCase() + '"], meta[property="PageMetadata:' + key.toLowerCase() + '"], meta[property="' + key.toLowerCase() + '"]');
            if (v4meta) {
                metaDataValue = v4meta.content;
            }
        }

        return metaDataValue;

    };

    var setMetaData = function setMetaData(key, value) {
        // Check if the element is set
        if (getMetaData(key)) {
            var meta = document.querySelector('meta[name="' + key + '"], meta[property="' + key + '"]');

            if (meta) {
                meta.content = value;
            }

            if (window.PageManager && PageManager.PageMetaData && PageManager.PageMetaData.Values && PageManager.PageMetaData.Values[key]) {
                PageManager.PageMetaData.Values[key] = value;
            }

        } else {
            // If not set create 
            var tmp = document.createElement("div");
            tmp.innerHTML = '<meta property="PageMetadata:' + key + '" content="' + encodeURI(value) + '" />';
            var childNodes = [].slice.call(tmp.childNodes);
            childNodes.forEach(function(ce, ci, ca) {
                document.head.appendChild(ce);
            });

            if (window.PageManager && PageManager.PageMetaData && PageManager.PageMetaData.Values && PageManager.PageMetaData.Values) {
                PageManager.PageMetaData.Values[key] = value;
            }
        }
    };

    var removeMetaData = function setMetaData(key) {
        // Check if the element is set
        if (getMetaData(key)) {
            var meta = document.querySelector('meta[name="' + key + '"]');
            document.head.removeChild(meta);

            if (PageManager.PageMetaData && PageManager.PageMetaData.Values && PageManager.PageMetaData.Values[key]) {
                delete PageManager.PageMetaData.Values[key];
            }
        }
    };

    var generateTimingVars = function generateTimingVars() {
        var Start = new Date().getTime();
        var h = window.performance || window.webkitPerformance || window.mozPerformance || window.msPerformance;
        if (h) {
            var k = h.timing;
            if (k) {
                var f = new Date().getTime();
                var a = k.responseStart - k.navigationStart;
                var e = k.responseEnd - k.responseStart;
                var i = f - k.responseEnd;
                var c = f - k.navigationStart;
                ClientTimings1Connect = a;
                ClientTimings2Load = e;
                ClientTimings3Render = i;
                ClientTimings4Duration = c;
            }
        }

    };


    var generatePurchaseID = function generatePurchaseID() {
        purchaseID = Math.round(Math.random() * 10000000).toString() + new Date().getTime();
        return purchaseID;
    };

    var getPurchaseID = function getPurchaseID() {
        return purchaseID;
    };

    var setAdTrack = function setAdTrack(val) {
        adtrack = val;
        return val;
    };

    var getAdTrack = function getAdTrack() {
        return adtrack;
    };

    var setPageView = function setPageView(val) {
        pageview = val;
        return val;
    };

    var getPageView = function getPageView() {
        return pageview;
    };

    var setCPD = function setCPD(val) {
        cpd.push(val);
        return val;
    };

    var getCPD = function getCPD() {
        var removeDups = cpd.filter(function(elem, pos, arr) {
            return arr.indexOf(elem) == pos;
        });
        return removeDups.join(';');
    };

    var setPVR = function setPVR(val) {
        pvr = val;
        return val;
    };

    var getPVR = function getPVR() {
        return pvr;
    };

    var setCCR = function setCCR(val) {
        ccr = val;
        return val;
    };

    var getCCR = function getCCR() {
        return ccr;
    };

    var getClickedRow = function getClickedRow() {
        return clickedRow;
    };

    var setClickedRow = function setClickedRow(val) {
        clickedRow = val;
        return val;
    };

    var getPID = function getPID() {
        var str = null;
        var pid = getCookies('brmp');

        if (pid) {
            pid = decodeURIComponent(pid);
            pidObj = JSON.parse(pid);

            if (pidObj.pid) {

                var jsonStr = JSON.stringify(pidObj.pid);
                str = jsonStr;
            }

        }

        return str;
    };

    var setEvents = function setEvents(val) {
        brevents.push(val);
        return val;
    };

    var getEvents = function getEvents() {
        return brevents.join(',');
    };

    var generateEvents = function generateEvents() {
        var aEvents = [],
            jEvents;
        var pvr = getPVR() || this.pvr;
        var ccr = getCCR() || this.ccr;

        if (brevents.length > 0) {
            aEvents = aEvents.concat(brevents);
        }

        if (this.events) {
            aEvents.concat(this.events.split(','));
        }

        if (document.querySelector(".change-search-modal.in,.br-learn-more-modal.in")) {
            // TODO: think of a better way to implement
            aEvents.push('event7');
        }

        if (pvr || ccr) {
            aEvents.push('purchase');
        }

        if (ccr) {
            aEvents.push('event6');
        }

        if (pvr) {
            aEvents.push('event23');
        }

        aEvents = aEvents.filter(function(elem, pos, arr) {
            return arr.indexOf(elem) == pos;
        });

        aEvents.sort();

        var eventStr = aEvents.join(',');

        return eventStr;
    };

    var generateProducts = function generateProducts() {
        var str = '';
        var pvr = getPVR() || this.pvr;
        var ccr = getCCR() || this.ccr;

        if (pvr && ccr) {
            str = pvr + ',' + ccr;
        } else if (pvr && !ccr) {
            str = pvr + ';';
        } else if (!pvr && ccr) {
            str = ccr;
        }

        return str;
    };


    var getAdKeywords = function getAdKeywords() {
        // TODO: make this work for v3 too
        var adkeys = getMetaData('AdKeyword');

        if (adkeys) {
            adkeys = adkeys.replace(/\(|\)|\-|\s/g, '');
            adkeys = adkeys.split(',');
        }

        return adkeys;
    };

    var getTitle = function getTitle() {
        return document.title;
    };

    var getJavascriptVersion = function getJavascriptVersion() {
        var j = '1.0',
            tm = new Date(),
            pn = 0,
            a, i, o, tcf;
        if (String && String.prototype) {
            j = '1.1';
            if (j.match) {
                j = '1.2';
                if (tm.setUTCDate) {
                    j = '1.3';
                    if (pn.toPrecision) {
                        j = '1.5';
                        a = [];
                        if (a.forEach) {
                            j = '1.6';
                            i = 0;
                            o = {};
                            tcf = new Function('o', 'var e,i=0;try{i=new Iterator(o)}catch(e){}return i');
                            i = tcf(o);
                            if (i && i.next) {
                                j = '1.7';
                            }
                        }
                    }
                }
            }
        }
        return j;
    };

    var generatePageName = function generatePageName() {
        var str = '';
        str = getMetaData('Site') + window.location.pathname.replace(/\//g, ">").replace(/\>+$/, '');

        if ((/\.com\/\?|\.com$/gi).test(window.location.href) || (window.location.pathname === "/") ) str += '>default.aspx';

        return str;
    };

    var generatepageID = function generatepageID() {
        var salt = window.location.pathname;
        var hashids = new Hashids(salt, 8);
        var id = hashids.encode(1, 2, 3);

        setMetaData('PageID', id);
        return id;

    };

    var getQueryString = function getQueryString() {
        var q = window.location.search;
        return (typeof q != 'undefined') ? q.replace('?', '') : '';
    };

    var cobrandType = function cobrandType() {
        var name = getMetaData('Site');
        var localCob = 'other';
        switch (name) {
            case 'aff':
            case 'brm':
            case 'br3':
            case 'br3_a':
            case 'br3_b':
            case 'br3_c':
            case 'br3_d':
            case 'br3_e':
            case 'can':
            case 'ffnd':
            case 'goocan':
            case 'gookeyword':
            case 'nltrack':
            case 'overkeyword':
            case 'sema':
            case 'semb':
                localCob = 'internal';
                break;
            default:
                localCob = 'partner';
        }
        return localCob;
    };

    var getCobrand = function getCobrand() {
        return (cobrandType() != 'internal') ? getMetaData('Site') : '';
    };

    var getHostName = function getHostName() {
        return window.location.hostname;
    };

    var collectSortBy = function collectSortBy() {
        var sb = getMetaData('SortBy');

        if (sb === null) {
            var elm = document.querySelector('.br-sort-by');

            if (elm && elm.tagName == "SELECT") {
                var opt = [].slice.call(elm.querySelectorAll('option')).filter(function(e) {
                    return e.selected === true;
                });
                sb = opt[0].textContent;
            }
        }

        return (sb) ? sb.trim() : '';
    };

    var detectDeviceType = function detectDeviceType() {
        var screenWith = window.screen.width;
        var mobileClass = document.querySelector('.mobile-view');

        return ((screenWith <= 640) || (mobileClass)) ? true : false;
    };

    var getBrTest = function getBrTest() {
        var datums = [];
        var nodes = [].slice.call(document.querySelectorAll('[data-test-description]'));

        // Build page data
        var nodeDatums = nodes.map(function(e, i) {
            var str = '';
            var data = e.dataset;
            var descriptionSplit = data.testDescription.split('/');
            var description = descriptionSplit[descriptionSplit.length - 1].replace(/\?.*/, '');
            var variationSplit = data.testVariation.split('/');
            var variation = variationSplit[variationSplit.length - 1].replace(/\?.*/, '');

            str = description + ':' + variation;

            return str;
        }).join(',');

        datums.push(nodeDatums);

        datums = datums.join(",");

        return (datums.length > 1) ? datums : '';

    };

    var getServerDuration = function getServerDuration() {
        return ServerDuration;
    };

    var getClientTimings1Connect = function getClientTimings1Connect() {
        return ClientTimings1Connect;
    };

    var getClientTimings2Load = function getClientTimings2Load() {
        return ClientTimings2Load;
    };

    var getClientTimings3Render = function getClientTimings3Render() {
        return ClientTimings3Render;
    };

    var getClientTimings4Duration = function getClientTimings4Duration() {
        return ClientTimings4Duration;
    };

    var getCookies = function getCookies(name) {
        var value, parts;
        if (name) {
            value = "; " + document.cookie;
            parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        } else {
            value = "; " + document.cookie;
            parts = value.split("; ");
            parts.shift();
            return parts.map(function(e) {
                var innerElm = e.split('=');
                return {
                    name: innerElm[0],
                    value: innerElm[1]
                };
            });
        }
    };

    var getVID = function getVID() {
        str = null;

        var brmp = getCookies('brmp');
        if (brmp) {
            var brmpStr = decodeURIComponent(brmp);
            var brmpObj = JSON.parse(brmpStr) || {};

            if (brmpObj.VID) {
                str = brmpObj.VID;
            }

        }

        return str;
    };

    var getSID = function getSID() {
        str = null;

        var brms = getCookies('brms');
        if (brms) {
            var brmsStr = decodeURIComponent(brms);
            var brmsObj = JSON.parse(brmsStr) || {};

            if (brmsObj.SID) {
                str = brmsObj.SID;
            }

        }

        return str;
    };

    var getLSID = function getLSID() {
        str = null;

        var brml = getCookies('brml');
        if (brml) {
            var brmlStr = decodeURIComponent(brml);
            var brmlObj = JSON.parse(brmlStr) || {};

            if (brmlObj.SID) {
                str = brmlObj.SID;
            }

        }

        return str;
    };

    var cleanHouse = function cleanHouse() {
        window.BRDataQ.forEach(function(e, i, a) {
            delete a[i];
        });

        window.BROmniQ.forEach(function(e, i, a) {
            delete a[i];
        });

        setPageView(false);
        setPVR(null);
        setAdTrack(null);
        setCCR(null);
        brevents = [];
        cpd = [];
        setPageView(false);
        setRI(null);
        setClickedRow(null);
        bankrate.omniture.setPEV2(null);

    };

    var isVisible = function isVisible(elem) {
        return elem.offsetWidth !== 0 && elem.offsetHeight !== 0;
    };

    var hasFilters = function hasFilters(ri) {
        return ri.filterCount > 0;
    };

    var getFilterValue = function getFilterValue(elm) {
        elm = document.querySelector(elm);
        var tag = elm.tagName;
        var filterValue;


        switch (tag) {
            case "SELECT":
                filterValue = [].slice.call(elm.querySelectorAll("option")).filter(function(e) {
                    return e.selected === true;
                })[0].textContent.trim();
                break;
            case "INPUT":
                filterValue = elm.value.trim();
                break;
            default:
                filterValue = elm.textContent.trim();
                break;
        }

        return filterValue;
    };

    var getRI = function getRI() {
        return brri;
    };

    var setRI = function setRI(val) {
        brri = val;
        return val;
    };

    var getImpressionIDTries = 0;
    var getImpressionID = function getImpressionID() {
        var impID = null;

        if (getMetaData('PageType') === null && getImpressionIDTries < 4) {
            setTimeout(getImpressionID, 100);
            getImpressionIDTries++;
            return false;
        }


        if (getMetaData('ImpressionID') === null) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    impID = xhttp.responseText;
                    setMetaData('ImpressionID', impID);
                }
            };
            xhttp.open("GET", "/ajax/dynamic-content/generate-impression-id/?type=text&pageType=" + getMetaData('PageType') + "&category=" + getMetaData('Category'));
            xhttp.send();
        } else {
            impID = getMetaData('ImpressionID');
        }

        return impID;

    };


    var countPaidListings = function countPaidListings() {
        var str = '';
        var rows;

        // Detect if there is a rate table 
        if (document.querySelector("[data-ri-table-group]")) {
            if (document.querySelector('.br-active-interstitial')) {
                rows = [].slice.call(document.querySelectorAll(".br-rate-table-row.paid"));
            } else {
                rows = [].slice.call(document.querySelectorAll(".br-rate-table-row.paid")).filter(isVisible);
            }
            rows = [].slice.call(document.querySelectorAll(".br-rate-table-row.paid")).filter(function(e) {
                return ((/omitcount/).test(e.className)) ? '' : e;
            });
            str = rows.length.toString();
        }

        return str;
    };

    var countEditorialListings = function countEditorialListings() {
        var str = '';
        var rows;

        // Detect if there is a rate table 
        if (document.querySelector("[data-ri-table-group]")) {

            if (document.querySelector('.br-active-interstitial')) {
                rows = [].slice.call(document.querySelectorAll(".br-rate-table-row.editorial,.br-cd ul.editorial"));
            } else {
                if (document.querySelector('.br-cd')) {
                    rows = [].slice.call(document.querySelectorAll(".br-cd ul.editorial"));
                } else {
                    rows = [].slice.call(document.querySelectorAll(".br-rate-table-row.editorial")).filter(isVisible);
                }
            }
            str = rows.length.toString();
        }

        return str;
    };

    var getQueryParameters = function getQueryParameters(str) {
        return (str || document.location.search).replace(/(^\?)/, '').split("&").map(function(n) {
            return n = n.split("="), this[n[0]] = n[1], this;
        }.bind({}))[0];
    };

    var buildRateImpression = function buildRateImpression() {
        var ri = {};

        var filters = [].slice.call(document.querySelectorAll('[data-ri-filtername]')).filter(function(e, i, a) {
            if (isVisible(e) || (e.dataset && e.dataset.riForce === "true")) {
                return true;
            }
        });

        var riTables = [].slice.call(document.querySelectorAll("[data-ri-table-group]"));
        var rows, pgc, rowdata, a, ridata, rijson;

        ri.filterCount = filters.length;
        ri.hasFilters = hasFilters(ri);
        ri.market = getMetaData('market');

        if (ri.hasFilters) {
            ri.filters = {};
            filters.forEach(function(e, i, a) {
                var filterName = filters[i].dataset.riFiltername;
                var filterValue = getFilterValue(filters[i].dataset.riFiltervalue);
                ri.filters[filterName] = filterValue;
            });
        }

        ri.productGroup = [];

        for (i = 0; i < riTables.length; i++) {
            rows = [];
            // pgc = [].slice.call(riTables[i].querySelectorAll('.br-rate-table-row')).filter(isVisible);
            var paid = [].slice.call(riTables[i].querySelectorAll(".br-rate-table-row.paid")).filter(function(e) {
                return ((/omitcount/).test(e.className)) ? '' : e;
            });

            if (document.querySelector('.br-cd')) {
                var editorial = [].slice.call(riTables[i].querySelectorAll(".br-cd ul.editorial"));
            } else {
                var editorial = [].slice.call(riTables[i].querySelectorAll(".br-rate-table-row.editorial"));
            }
            pgc = paid.concat(editorial);
            
            for (a = 0; a < pgc.length; a++) {
                rowdata = pgc[a].dataset;
                ridata = {};
                for (var key in rowdata) {
                    if ((/^ri|sponsored|rate/).test(key)) {
                        ridata[key.replace(/^ri/, '').toLowerCase()] = rowdata[key];
                    }
                }

                if (Object.keys(ridata).length !== 0) {
                    rows.push(ridata);
                }
            }

            ri.productGroup[i] = {
                name: riTables[i].dataset.riTableGroup,
                rowCount: rows.length,
                rows: rows
            };
        }

        rijson = JSON.stringify(ri);

        brri = rijson;

        return rijson;
    };

    generateTimingVars();
    getImpressionID();
    generatepageID();

    document.addEventListener("DOMContentLoaded", buildRateImpression);

    return {
        getMetaData: getMetaData,
        setMetaData: setMetaData,
        removeMetaData: removeMetaData,
        getTitle: getTitle,
        generatePageName: generatePageName,
        getQueryString: getQueryString,
        cobrandType: cobrandType,
        getCobrand: getCobrand,
        getHostName: getHostName,
        collectSortBy: collectSortBy,
        detectDeviceType: detectDeviceType,
        getBrTest: getBrTest,
        getAdKeywords: getAdKeywords,
        setPVR: setPVR,
        getPVR: getPVR,
        setCCR: setCCR,
        getCCR: getCCR,
        setEvents: setEvents,
        getEvents: getEvents,
        setAdTrack: setAdTrack,
        getAdTrack: getAdTrack,
        generatePurchaseID: generatePurchaseID,
        getPurchaseID: getPurchaseID,
        setPageView: setPageView,
        getPageView: getPageView,
        getServerDuration: getServerDuration,
        getClientTimings1Connect: getClientTimings1Connect,
        getClientTimings2Load: getClientTimings2Load,
        getClientTimings3Render: getClientTimings3Render,
        getClientTimings4Duration: getClientTimings4Duration,
        getJavascriptVersion: getJavascriptVersion,
        getCookies: getCookies,
        getVID: getVID,
        getSID: getLSID,
        getLSID: getLSID,
        getPID: getPID,
        generateEvents: generateEvents,
        generateProducts: generateProducts,
        buildRateImpression: buildRateImpression,
        getRI: getRI,
        cleanHouse: cleanHouse,
        getQueryParameters: getQueryParameters,
        getImpressionID: getImpressionID,
        countPaidListings: countPaidListings,
        countEditorialListings: countEditorialListings,
        setClickedRow: setClickedRow,
        getClickedRow: getClickedRow,
        generatepageID: generatepageID,
        setCPD: setCPD,
        getCPD: getCPD,
    };
}));