(function(root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory);
    } else if (typeof exports === "object") {
        module.exports = factory(require("jquery"));
    } else {
        root.bankrate = root.bankrate || {};
        root.bankrate.events = factory(root.jQuery);
    }
}(this, function($) {
    
    var extend = function extend() {
        for (var i = 1; i < arguments.length; i++)
            for (var key in arguments[i])
                if (arguments[i].hasOwnProperty(key))
                    arguments[0][key] = arguments[i][key];
        return arguments[0];
    };

    var hasClass = function hasClass(element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    };

    var doc = $(document);    

    //Passing bootstrap events to pure javascript 
    doc.on('shown.bs.modal', function(event) {
        var customevent = new Event('shown.bs.modal');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    }).on('show.bs.modal', function(event) {
        var customevent = new Event('show.bs.modal');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    }).on('hide.bs.modal', function(event) {
        var customevent = new Event('hide.bs.modal');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    }).on('hidden.bs.modal', function(event) {
        var customevent = new Event('hidden.bs.modal');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    }).on('slide', function(event) {
        var customevent = new Event('slide');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    }).on('slideStop', function(event) {
        var customevent = new Event('slideStop');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    }).on('bankrate:track:CCRSent', function (event) {
            // TODO: create this in ratetable.js
            var customevent = new Event('bankrate:track:CCRSent');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    });

    // create custom events
    var filteredTable = function filteredTable(event){
        var customevent = new Event('bankrate:track:filteredtable');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    };

    var closeLightBox = function closeLightBox(event){
        var customevent = new Event('bankrate:track:closelightbox');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    };

    var openLightBox = function openLightBox(event){
        var customevent = new Event('bankrate:track:openlightbox');
            customevent = extend(customevent,event);

            document.dispatchEvent(customevent);
    };

    // Rate Table revenue click
    document.addEventListener('click', function(event){ 
        if(hasClass(event.target, 'track')){
            var customevent = new Event('bankrate:track:revenueclick');
            customevent = extend(customevent, event);

            event.target.dispatchEvent(customevent);      
        }
    });

    return {
        filteredTable: filteredTable,
        closeLightBox: closeLightBox,
        openLightBox: openLightBox
    };
}));