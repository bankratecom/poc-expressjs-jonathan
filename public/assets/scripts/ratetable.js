(function(root, factory) {
    if (typeof define === "function" && define.amd) {
        define(['meta', 'tracking', 'adgpt'], factory);
    } else if (typeof exports === "object") {
        module.exports = factory(require("meta"), require('tracking'), require('adgpt'));
    } else {
        root.bankrate = root.bankrate || {};
        root.bankrate.meta = root.bankrate.meta || {};
        root.bankrate.omniture = root.bankrate.omniture || {};
        root.bankrate.tracking = root.bankrate.tracking || {};
        root.bankrate.adgpt = root.bankrate.adgpt || {};
        root.bankrate.ratetable = factory(root.bankrate.meta, root.bankrate.omniture, root.bankrate.tracking, root.bankrate.adgpt);
    }
}(this, function(meta, omniture, tracking, ads) {

    var extend = function extend() {
        for (var i = 1; i < arguments.length; i++)
            for (var key in arguments[i])
                if (arguments[i].hasOwnProperty(key))
                    arguments[0][key] = arguments[i][key];
        return arguments[0];
    };

    var closest = function closest(el, fn) {
        while (el) {
            if (fn(el)) return el; 
            el = el.parentNode;
        }
    };

    var processRevenue = function processRevenue(event) {
        generateCCR(event.target);
        setClickType(event.target);
        setEventType(event.target);

        //only for a open lightbox for now
        if(event.target && event.target.dataset && event.target.dataset.eventtype === 'open') sendEP(event.target);
    };

    var setClickType = function setClickType(elm){
        var clicktype = null;

        if(elm && elm.dataset && elm.dataset.clicktype) {
            clicktype = elm.dataset.clicktype;
            meta.setMetaData('ClickType',clicktype);
        }

        return clicktype;
    };

    var setEventType = function setEventType(elm){
        var eventtype = null;

        if(elm && elm.dataset && elm.dataset.eventtype) {
            eventtype = elm.dataset.eventtype;
            meta.setMetaData('EventType', eventtype);
        }

        return eventtype;
    };

    var generateCCR = function extractCCR(elm) {
        var CCRString = '';

        var cpcData = extractCPCData(elm);

        if(cpcData) {
            CCRString = 'CCR;p_' + cpcData.product + ';1;' + cpcData.unitPrice ;

            meta.setCCR(CCRString);
        }

        return CCRString;
    };

    var extractCPCData = function extractCPCData(elm){
        var cpcData = null, cpcString, cpcAfterPipe, cpcDataArray, override = false;
        
        if (elm && elm.dataset && elm.dataset.cpc) {
            cpcString = event.target.dataset.cpc;
            

        } else {
            var cpcElm = closest(elm, function(el){ 
                return el && el.dataset && el.dataset.cpc;
            });
            
            if(cpcElm && cpcElm.dataset && cpcElm.dataset.cpc) {
                cpcString = cpcElm.dataset.cpc;
            }
        }


        if (elm && elm.dataset && elm.dataset.choverride) {
            override = (elm.dataset.choverride === '1') ? true : false ;
        } 

        if(cpcString) {
            cpcAfterPipe = cpcString.split('||');
            
            if(cpcAfterPipe.length > 0) {
                cpcDataArray = cpcAfterPipe[1].split(',');

                if(cpcDataArray.length > 0) {
                    cpcData = {};
                    cpcData.servingInstitution = cpcDataArray[0];
                    cpcData.market = cpcDataArray[1];
                    cpcData.product = cpcDataArray[2];
                    cpcData.unitPrice = (override) ? '0' :  cpcDataArray[3];
                }
            }
        }      

        return cpcData;
    };

    var setClickedRowPosition =  function setClickedRowPosition(event){
        var position = 1;
        var cpcElm = closest(event.target, function(el){ 
            return el && el.dataset && el.dataset.cpc;
        });

        if(cpcElm) {
            var nodeList = [].slice.call(cpcElm.parentNode.children);
            position = nodeList.indexOf(cpcElm) + 1;
        }

        meta.setClickedRow(position);

    };

    var sendEP = function sendEP(elm){
        // TODO: move redirect ep into this file form old tracking
        var cpcData = extractCPCData(elm);
        
        if(cpcData){
            tracking.redirectEP(elm);
        }
    };

    var closeModal = function closeModal() {
        meta.setEvents('event9');        
        
        // if no ads send omniture and datacollector anyway
        if (ads.activeSlots.count === 0) {
            var omniObj = {};
            var dataObj = {};
            window.BROmniQ.push(omniObj);
            window.BROmniQ.push("track");
            window.BRDataQ.push(dataObj);
            window.BRDataQ.push("track");            
        } else {
            ads.refreshAds(ads.activeSlots.refreshingAds);
        }

    };

    var openModal =  function openModal (event){
        var dpfAds = document.querySelector(".modal.in [data-dfp]");
        var modal = document.querySelector(".modal.in");

        meta.setEvents('event7');

        if((/change/gmi).test(modal.className) || (/change/gmi).test(modal.id)) {
            omniture.setPEV2('ChangeSearchLightbox');
        }

        if((/info|more/gmi).test(modal.className) || (/info|more/gmi).test(modal.id)) {
            omniture.setPEV2('learnmorelightbox');
        }

        // if no ads send omniture and datacollector anyway
        if (!dpfAds) {
            var omniObj = {};
            var dataObj = {};
            window.BROmniQ.push(omniObj);
            window.BROmniQ.push("track");
            window.BRDataQ.push(dataObj);
            window.BRDataQ.push("track");            
        } else {
            ads.runModalAds();
        }

    };

    var sendRateTableWidgetTracking = function sendRateTableWidgetTracking (){        
            meta.setPageView(false);
            omniture.setPEV2('searchButton');
            meta.buildRateImpression();
            meta.setEvents('event28');
            window.BROmniQ.push({});
            window.BROmniQ.push("track");
            window.BRDataQ.push({});
            window.BRDataQ.push("track");
            meta.setPageView(true);
            meta.cleanHouse();        
    };

    document.addEventListener('bankrate:track:revenueclick', processRevenue, true, true);
    document.addEventListener('bankrate:track:revenueclick', setClickedRowPosition, true, true);
    document.addEventListener('bankrate:track:closelightbox', closeModal);
    document.addEventListener('bankrate:track:filteredtable', ads.refreshAds);
    document.addEventListener('bankrate:track:openlightbox', openModal);

    return {
        processRevenue: processRevenue,
        extractCPCData: extractCPCData,
        generateCCR: generateCCR,
        sendEP: sendEP,
        sendRateTableWidgetTracking: sendRateTableWidgetTracking
    };
}));