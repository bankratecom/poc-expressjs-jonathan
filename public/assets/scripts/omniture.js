(function(root, factory) {
    if (typeof define === "function" && define.amd) {
        define(["json/EnvConfig", "meta", "vendor/s_code_brm"], factory);
    } else if (typeof exports === "object") {
        module.exports = factory(require("json/EnvConfig"), require("meta"), require("vendor/s_code_brm"));
    } else {
        root.bankrate = root.bankrate || {};
        root.bankrate.meta = root.bankrate.meta || {};
        root.s_gi = root.s_gi || {};
        root.bankrate.omniture = factory(root.EnvConfig, root.bankrate.meta, root.s_gi);
    }
}(this, function(config, meta, scode) {
    window.BROmniQ = window.BROmniQ || [];

    var endpoint = "/bankrate-omniture.htm";
    var suite = EnvConfig.OmnitureSuite;
    var pev2 = null;

    // Extend push 
    window.BROmniQ.push = function() {

        if (arguments[0] === "track") {
            processQueue.call(this);
        }

        return Array.prototype.push.apply(this, arguments);
    };

    var extend = function extend() {
        for (var i = 1; i < arguments.length; i++)
            for (var key in arguments[i])
                if (arguments[i].hasOwnProperty(key))
                    arguments[0][key] = arguments[i][key];
        return arguments[0];
    };

    var initiateOmniObject = function initiateOmniObject() {
        return varsWhiteList(scode(suite));
    };

    var extendPageLoadEvents = function extendPageLoadEvents(obj) {
        if (meta.getPageView() || obj.pageview) {
            var currentEvents = (obj.events) ? obj.events.split(',') : [];
            var testHref = window.location.href;
            var queryParams = meta.getQueryParameters();


            if ((/ec_id/g).test(testHref)) {
                obj.campaign = queryParams.ec_id;
                currentEvents.push('event1');
            }

            if ((/pc_id/g).test(testHref)) {
                obj.eVar26 = queryParams.pc_id;
                currentEvents.push('event2');
            }

            if ((/ic_id/g).test(testHref)) {
                obj.eVar27 = queryParams.ic_id;
                currentEvents.push('event3');
            }

            if ((/ef_id/g).test(testHref)) {
                obj.eVar44 = queryParams.ef_id;
                currentEvents.push('event10');
            }
            if ((/ab1_id/g).test(testHref)) {
                obj.eVar29 = queryParams.ab1_id;
                currentEvents.push('event16');
            }

            if (currentEvents.length > 0) obj.events = currentEvents.join(',');
        }

        return obj;

    };

    var varsWhiteList = function varsWhiteList(obj) {
        var peArray = ['purchaseID', 'transactionID', 'events', 'products'];
        for (var i = 1; i < 150; i++) {
            peArray.push('prop' + i);
            peArray.push('eVar' + i);
        }

        peArray.push('list1', 'list2');

        var peStr = peArray.join(',');

        obj.linkTrackVars = peStr;

        return obj;

    };

    var extendOmniObject = function extendOmniObject(omni, obj) {
        return extend(omni, obj);
    };

    var processQueue = function processQueue() {
        this.forEach(function(e, i, a) {
            if (typeof e === "object") {
                var om = populateOmniture(extendPageLoadEvents(extendOmniObject(initiateOmniObject(), e)));
                sendOmniture.call(om);
            }
        });


    };

    var sendOmniture = function sendOmniture() {
        if (meta.getPageView() || this.pageview) {
            this.t();
        } else {
            this.tl(null, 'o', pev2);
        }

        sendOmnitureSentSignal();
    };

    var sendOmnitureSentSignal = function sendOmnitureSentSignal(){

         var customevent = new CustomEvent('bankrate:track:omnitureSent');

        document.dispatchEvent(customevent);
    };

    var getTitle = function getTitle() {
        return document.title;
    };


    var generatePageName = function generatePageName() {
        var str = '';
        str = getMetaData('Site') + window.location.pathname.replace(/\//g, ">").replace(/\>+$/, '');

        if ((/\.com\/\?|\.com$/gi).test(window.location.href)) str += '>default.aspx';

        return str;
    };

    var getPEV2 = function getPEV2() {
        return pev2;
    };

    var setPEV2 = function setPEV2(val) {
        pev2 = val;
        return pev2;
    };

    var getQueryString = function getQueryString() {
        var q = window.location.search;
        return (typeof q != 'undefined') ? q.replace('?', '') : '';
    };

    var getHostName = function getHostName() {
        return window.location.hostname;
    };

    var collectSortBy = function collectSortBy() {
        var sb = getMetaData('SortBy');
        return (sb) ? sb.trim() : '';
    };

    var detectDeviceType = function detectDeviceType() {
        return ((/mobile-view/).test(document.body.className)) ? 'phone' : 'desktop';
    };

    var getBrTest = function getBrTest() {
        var datums = [];
        var nodes = [].slice.call(document.querySelectorAll('[data-test-description]'));

        // Build page data
        var nodeDatums = nodes.map(function(e, i) {
            var str = '';
            var data = e.dataset;
            var descriptionSplit = data.testDescription.split('/');
            var description = descriptionSplit[descriptionSplit.length - 1].replace(/\?.*/, '');
            var variationSplit = data.testVariation.split('/');
            var variation = variationSplit[variationSplit.length - 1].replace(/\?.*/, '');

            str = description + ':' + variation;

            return str;
        }).join(',');

        datums.push(nodeDatums);

        datums = datums.join(",");

        return (datums.length > 1) ? datums : '';

    };

    var generatePEV2 = function generatePEV2() {
        var pev2 = getPEV2() || this.pev2 || null;

        setPEV2(pev2);

        return pev2;
    };

    var getURLWithoutQuery = function getURLWithoutQuery() {
        return window.location.href.split('?')[0];
    };

    var populateOmniture = function populateOmniture(sObject) {
        meta.generatePurchaseID();
        var omnitureVarsFromMetaData = {
            'pageName,eVar21,prop22': {
                fc: meta.generatePageName
            },
            'purchaseID': {
                fc: meta.getPurchaseID
            },
            'transactionID': {
                fc: meta.getPurchaseID
            },
            'eVar62': {
                fc: meta.getPurchaseID
            },
            'events': {
                fc: meta.generateEvents.bind(sObject)
            },
            'products': {
                fc: meta.generateProducts.bind(sObject)
            },
            'eVar1,eVar14,prop1,prop14': {
                meta: 'Site'
            },
            'eVar2,prop2': {
                meta: 'Category'
            },
            'eVar3,prop3': {
                meta: 'Channel'
            },
            'eVar4,prop4': {
                meta: 'Categories'
            },
            'eVar5,prop5': {
                meta: 'SubCategories'
            },
            'eVar6,prop24,prop6': {
                fc: meta.getQueryString
            },
            'eVar8,prop8': {
                meta: 'Location'
            },
            'eVar9,prop9': {
                fc: meta.cobrandType
            },
            'eVar10,prop10': {
                meta: 'EventName'
            },
            'eVar11,prop11': {
                meta: 'Author'
            },
            'eVar12,prop12': {
                meta: 'PageNumber'
            },
            'eVar13,prop13': {
                fc: meta.getCobrand
            },
            'evar15,prop15': {
                str: ''
            },
            'eVar16,prop16': {
                meta: 'AdArea'
            },
            'eVar17,prop17': {
                meta: 'AdKeyword'
            },
            'eVar22': {
                meta: 'FicoScore'
            },
            'eVar23,prop23': {
                fc: meta.getHostName
            },
            'eVar24': {
                meta: 'PercentDown'
            },
            'eVar25,prop25': {
                meta: 'PageType'
            },
            'eVar31,prop31': {
                fc: meta.getServerDuration
            },
            'eVar32,prop32': {
                fc: meta.getClientTimings1Connect
            },
            'eVar33,prop33': {
                fc: meta.getClientTimings2Load
            },
            'eVar34,prop34': {
                fc: meta.getClientTimings3Render
            },
            'eVar35,prop35': {
                fc: meta.getClientTimings4Duration
            },
            'eVar36': {
                meta: 'ServingInstitution'
            },
            'eVar38': {
                meta: 'Position'
            },
            'eVar39': {
                meta: 'ClickType'
            },
            'eVar40,eVar41': {
                meta: 'Market'
            },
            'eVar42': {
                meta: 'Product'
            },
            'eVar45': {
                meta: 'SponsoredLender'
            },
            'eVar46': {
                fc: meta.collectSortBy
            },
            'eVar49,prop49': {
                fc: meta.detectDeviceType
            },
            'eVar50': {
                fc: meta.getImpressionID
            },
            'eVar68,prop68': {
                fc: getURLWithoutQuery
            },
            'pev2': {
                fc: generatePEV2.bind(sObject)
            },
            'list1': {
                fc: meta.getBrTest
            },
            'prop21': {
                fc: meta.getTitle
            },
            'eVar81': {
                fc: meta.getCPD
            },
            'eVar84': {
                fc: meta.countPaidListings
            },
            'eVar85': {
                fc: meta.countEditorialListings
            },
            'eVar86': {
                fc: meta.getClickedRow
            }
        };

        var omniObject = sObject;

        for (var key in omnitureVarsFromMetaData) {
            var omiItem = key.replace(/\s/g, '').split(','),
                value = '';

            if (typeof omnitureVarsFromMetaData[key].meta != 'undefined' && meta.getMetaData(omnitureVarsFromMetaData[key].meta))
                value = meta.getMetaData(omnitureVarsFromMetaData[key].meta);

            if (typeof omnitureVarsFromMetaData[key].fc != 'undefined')
                value = omnitureVarsFromMetaData[key].fc();

            if (typeof omnitureVarsFromMetaData[key].str != 'undefined')
                value = omnitureVarsFromMetaData[key].str;

            for (i = 0; i < omiItem.length; i++) {
                omniObject[omiItem[i]] = value;
            }
        }

        return omniObject;

    };

    return {
        populateOmniture: populateOmniture,
        initiateOmniObject: initiateOmniObject,
        getPEV2: getPEV2,
        setPEV2: setPEV2
    };
}));