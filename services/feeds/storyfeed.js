var baseUri = 'http://ec2-54-69-58-248.us-west-2.compute.amazonaws.com/wp-json/wp/v2/';
var request = require('sync-request');
var trim = require('trim');
var striptags = require('striptags');

exports.shortendBlurb = function shortendBlurb(str, max){
	var yourString = str;
	var maxLength = max;

	//trim the string to the maximum length
	var trimmedString = yourString.substr(0, maxLength);

	//re-trim if we are in the middle of a word
	trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));

	return trimmedString;
};


exports.getResponse = function getResponse(uri){		
	uri = uri || '';
	var res = request('GET', baseUri + uri, {cache: 'file'});

	return res.getBody('utf8');
};

exports.getLatestPosts = function getLatestPosts(max) {
	max = max || 4;
	finalPosts = [];
    posts = JSON.parse(exports.getResponse('posts/?per_page=' + max));

    posts.forEach(function(e,i,a){
		finalPosts.push({
		                'title' : e.title.rendered,
		                'excerpt' : striptags(trim(e.excerpt.rendered)),
		                'little_excerpt' : exports.shortendBlurb(striptags(trim(e.excerpt.rendered)), 100),
		                'feature_image' : exports.getMediaUrl(e.featured_media),
		                'link' : e.link,
		                'slug' : e.slug
		            });
    });


    return finalPosts;
};

exports.getMediaUrl = function getMediaUrl(id) {
	var featuredMedia, res, rendered;

	if(id) {
		featuredMedia = exports.getResponse('media/' + id);
		res = JSON.parse(featuredMedia);
		rendered = res.guid.rendered;
	}

	
	return rendered;
};

exports.getPostBySlug = function getPostBySlug(slug) {
    var posts = JSON.parse(exports.getResponse('posts/?per_page=1&slug=' + slug));

    if (posts) {
        return {
            'title' : posts[0].title.rendered,
            'excerpt' : posts[0].excerpt.rendered,
            'content' : posts[0].content.rendered,
            'link' : posts[0].link,
            'date' : posts[0].date,
            'feature_image': exports.getMediaUrl(posts[0].featured_media),
            'author' : exports.getAuthor(posts[0].author)
        };
    }

    return {};
};

exports.getAuthor =  function getAuthor(id){
   var author = JSON.parse(exports.getResponse('users/' + id));

   return author ;
};

