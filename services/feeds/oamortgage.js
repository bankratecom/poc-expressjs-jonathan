var baseUri = 'http://aws.bankrate.com/';
var request = require('sync-request');
var parseString = require('xml2js').parseString;

exports.getResponse = function getResponse(uri){		
		uri = uri || '';
		var res = request('GET', baseUri + uri, {cache: 'file'});

		return res.getBody('utf8');
};

exports.get = function get(productIds, marketId){
        var feedResponse = exports.getResponse('oa/mortgage/' + productIds + (marketId ? '/' . marketId : ''));

        if (feedResponse.substring( 0, 9) == '<response') {
            var xml = feedResponse;

            parseString(xml, function (err, result) {
				console.dir(result);
				xml = result;
			});

            return xml;
        }

        var res = JSON.parse(feedResponse);

        return res;
 };	
